package com.cs442.to_dolist;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Xml;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;



@SuppressLint("ShowToast")
public class ToDoListActivity extends Activity implements NewItemFragment.OnNewItemAddedListener{

	
	//This is a test
	 private ArrayList<ToDoItem> todoItems;
	 private ToDoItemAdapter aa;
	 String filepath=Environment.getExternalStorageDirectory()+File.separator+"ToDoList.xml";
	 
	    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		Context context=getApplicationContext();
		
		File toImport= new File(filepath);
		try {
		//	xmlParse(toImport);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(context);
		setContentView(R.layout.activity_main);
		
	    FragmentManager fm= getFragmentManager();
	    ToDoListFragment todoListFragment= (ToDoListFragment) fm.findFragmentById(R.id.ToDoListFragment);
	   
	    
	    todoItems=new ArrayList<ToDoItem>();
	    
	 		
	    updateFromPreferences();
	    
	    
	    
	   
	   int resID=R.layout.todolist_item;
	    aa = new ToDoItemAdapter(this, resID, todoItems);
	    
	 
	    // Bind the Array Adapter to the List View
	    
	    todoListFragment.setListAdapter(aa);
				
			

	
	    
	    
	}
	
	public void onStop(){
		super.onStop();
		try {
			String xml=writeToXML(todoItems);
			toFile(xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void onResume(){
		super.onResume();
		File toImport= new File(filepath);
		try {
			xmlParse(toImport);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	 public void onStart(){
		super.onStart();
	
	} 
	
	public void onNewItemAdded(String newItem){
		ToDoItem newToDoItem=new ToDoItem(newItem);
		newItem=newItem.trim();
		if(!newItem.equals("")){
			todoItems.add(0, newToDoItem);
			aa.notifyDataSetChanged();
		}
		try {
			String xml=writeToXML(todoItems);
			toFile(xml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("SimpleDateFormat")
	public void xmlParse(File aFile) throws Exception{
	
		FileInputStream fis = new FileInputStream(aFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        String data = "";
        String line;

        while((line = br.readLine()) != null)
        {
                data += line;
        		

        }
        fis.close();
        br.close();

		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yy");
		 XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
         factory.setNamespaceAware(true);
         XmlPullParser xpp = factory.newPullParser();
         String dateToAdd;
         String taskToAdd = null;
         Date parseDate;
         StringReader sr = new StringReader(data);
         xpp.setInput(sr);
         int eventType = xpp.getEventType();
         while(eventType != XmlPullParser.END_DOCUMENT)
         {
                 if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("task"))
                 {
                         taskToAdd = xpp.nextText();

                         eventType = xpp.next();
                 }
                 else if(eventType == XmlPullParser.START_TAG && xpp.getName().equals("date"))
                         {
                     			dateToAdd=xpp.nextText();
                                 

                                 ToDoItem newItem=new ToDoItem(taskToAdd, dateToAdd);
                                 todoItems.add(newItem);
                                 aa.notifyDataSetChanged();
                                 eventType=xpp.next();
                         }
                 
                 else {
                 eventType = xpp.next();
         
           
              }
         }
         }
 
		
	
		public static String writeToXML(ArrayList<ToDoItem> items) throws Exception {
		    XmlSerializer xmlSerializer = Xml.newSerializer();
		    StringWriter writer = new StringWriter();
		    int i=items.size();
		    xmlSerializer.setOutput(writer);
		    // start DOCUMENT
		    xmlSerializer.startDocument("UTF-8", true);
		    xmlSerializer.startTag("", "TaskList");
		    int j=0;
		    while(j<i){
		    	
		    
		    // open tag: <author>
		    xmlSerializer.startTag("", "task");
		    xmlSerializer.text(items.get(j).getTask());
		    // close tag: </author>
		    xmlSerializer.endTag("", "task");
		 
		    // open tag: <date>
		    xmlSerializer.startTag("", "date");
		    @SuppressWarnings("deprecation")
			String Date=items.get(j).getCreated();
		    xmlSerializer.text(Date);
		    // close tag: </date>
		    xmlSerializer.endTag("", "date");
		    j++;
		    }
		   
		    xmlSerializer.endTag("", "TaskList");
		 
		    // end DOCUMENT
		    xmlSerializer.endDocument();
		 
		    return writer.toString();
		}
	public void toFile(String xmlData) throws IOException{
		File XMLFile=new File(filepath);
		FileWriter fw=new FileWriter(XMLFile);
		fw.write(xmlData);
		fw.close();
	}
	
	static final private int MENU_PREFERENCES = Menu.FIRST+1;
	static final private int MENU_DELETE= Menu.FIRST+2;
	  private static final int SHOW_PREFERENCES = 1;
	@Override
	  public boolean onCreateOptionsMenu(Menu menu) {
	    super.onCreateOptionsMenu(menu);

	    menu.add(0, MENU_PREFERENCES, Menu.NONE, R.string.menu_preferences);
	    menu.add(0, MENU_DELETE, Menu.NONE, R.string.menu_delete);

	    return true;
	  }

	  public boolean onOptionsItemSelected(MenuItem item){
	    super.onOptionsItemSelected(item);
	    switch (item.getItemId()) {

	      case (MENU_PREFERENCES): {
	        Class c = Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB ?    
	          PreferencesActivity.class : FragmentPreferences.class;
	        Intent i = new Intent(this, c);

	        startActivityForResult(i, SHOW_PREFERENCES);
	        return true;
	      }
	      case (MENU_DELETE) : {
	    	  todoItems.clear();
	    	  aa.notifyDataSetChanged();
	    	  
	      }
	    }
	    return false;
	  }

	  public String noteColor;
	  

	  private void updateFromPreferences() {
	    Context context = getApplicationContext();
	    SharedPreferences prefs = 
	      PreferenceManager.getDefaultSharedPreferences(context);
	    Resources res=getResources();
	    int theColor;  
	    theColor=res.getColor(R.color.notepad_paper);
	    noteColor=prefs.getString(PreferencesActivity.PREF_NOTE_COLOR, "Cyan");

	    
	    if (noteColor.equals("White")){
	    	
	    	theColor=res.getColor(R.color.white);
	    	
	    	
	    }
	    else if (noteColor.equals("Cyan")){
	    	theColor=res.getColor(R.color.cyan);
	    }
	    LinearLayout thisLayout=(LinearLayout)findViewById(R.id.theBackground);
	    thisLayout.setBackgroundColor(theColor);
	   	
	    
	  }
	
	  
	  public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    if (requestCode == SHOW_PREFERENCES){
	      updateFromPreferences();
	      
	      aa.notifyDataSetChanged();
	    }
	  }

}
