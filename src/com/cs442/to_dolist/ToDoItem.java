package com.cs442.to_dolist;


import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressLint("SimpleDateFormat")
public class ToDoItem {

	String task;
	Date created;
	
	public String getTask(){
		return task;
	}
	public String getCreated() {
		return new SimpleDateFormat("dd/MM/yy").format(created);
	}
	public void setTask(String _task){
		task=_task;
	}
	public ToDoItem(String _task){
		task=_task;
		created=new Date(System.currentTimeMillis());
	}
	public ToDoItem(String _task, String _created){
		task=_task;
		created = new Date(System.currentTimeMillis());
	}
	
	
	@Override
	public String toString(){
		SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yy");
		String dateString=sdf.format(created);
		return "(" + dateString + ") " + task;
	}
}
