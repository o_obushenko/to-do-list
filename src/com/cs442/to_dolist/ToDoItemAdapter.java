package com.cs442.to_dolist;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


@SuppressLint("SimpleDateFormat")
public class ToDoItemAdapter extends ArrayAdapter<ToDoItem>{
	
	int resource;
	
	public ToDoItemAdapter(Context context, int resource, List<ToDoItem> items){
		super(context,resource,items);
		this.resource=resource;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		LinearLayout todoView;
		
		ToDoItem item=getItem(position);
		
		String taskString= item.getTask();
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yy");
		Date createdDate = null;
		try {
			createdDate = sdf.parse(item.getCreated());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String dateString = sdf.format(createdDate);
		
		if (convertView == null){
			todoView= new LinearLayout(getContext());
			String inflater=Context.LAYOUT_INFLATER_SERVICE;
			LayoutInflater li;
			li= (LayoutInflater)getContext().getSystemService(inflater);
			li.inflate(resource, todoView, true);
		}
		else {
			todoView=(LinearLayout) convertView;
		}
		
		TextView dateView=(TextView) todoView.findViewById(R.id.rowDate);
		TextView taskView=(TextView) todoView.findViewById(R.id.row);
		
		dateView.setText(dateString);
		taskView.setText(taskString);
		
		return todoView;
		}
	}


